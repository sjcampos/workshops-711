const mongoose = require('mongoose');

const StudentSchema = mongoose.Schema({
    firstname: String,
    lastname: String,
    email: String,
    address: String,
}, {
    timestamps: true
});

module.exports = mongoose.model('Students', StudentSchema);