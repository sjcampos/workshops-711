const student = require('../models/student.model.js');
// Retrieve and return all student from the database.
exports.findAll = (req, res) => {
    student.find()
        .then(student => {
            res.send(student);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Something went wrong while getting list of students."
            });
        });
};
// Create and Save a new student
exports.create = (req, res) => {
    // Validate request
    if (!req.body) {
        return res.status(400).send({
            message: "Please fill all required field"
        });
    }
    // Create a new student
    const s = new student({
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        address: req.body.address
    });
    // Save student in the database
    s.save()
        .then(data => {
            res.send(data);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Something went wrong while creating new student."
            });
        });
};
// Find a single student with a id
exports.findOne = (req, res) => {
    student.findById(req.params.id)
        .then(student => {
            if (!student) {
                return res.status(404).send({
                    message: "student not found with id " + req.params.id
                });
            }
            res.send(student);
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "student not found with id " + req.params.id
                });
            }
            return res.status(500).send({
                message: "Error getting student with id " + req.params.id
            });
        });
};
// Update a student identified by the id in the request
exports.update = (req, res) => {
    // Validate Request
    if (!req.body) {
        return res.status(400).send({
            message: "Please fill all required field"
        });
    }
    // Find student and update it with the request body
    student.findByIdAndUpdate(req.params.id, {
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            email: req.body.email,
            address: req.body.address
        }, { new: true })
        .then(student => {
            if (!student) {
                return res.status(404).send({
                    message: "student not found with id " + req.params.id
                });
            }
            res.send(student);
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "student not found with id " + req.params.id
                });
            }
            return res.status(500).send({
                message: "Error updating student with id " + req.params.id
            });
        });
};
// Delete a student with the specified id in the request
exports.delete = (req, res) => {
    student.findByIdAndRemove(req.params.id)
        .then(student => {
            if (!student) {
                return res.status(404).send({
                    message: "student not found with id " + req.params.id
                });
            }
            res.send({ message: "student deleted successfully!" });
        }).catch(err => {
            if (err.kind === 'ObjectId' || err.name === 'NotFound') {
                return res.status(404).send({
                    message: "student not found with id " + req.params.id
                });
            }
            return res.status(500).send({
                message: "Could not delete student with id " + req.params.id
            });
        });
};