const express = require('express');
const bodyParser = require('body-parser');
const jwt = require("jsonwebtoken");
const theSecretKey = '123'; //pasarlo a un archivo
// create express app
const app = express();
// Setup server port
const port = process.env.PORT || 4000;
const basicAuth = require('express-basic-auth')
const {
    base64decode
} = require('nodejs-base64');

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))
    // parse requests of content-type - application/json
app.use(bodyParser.json())
    // Configuring the database
const dbConfig = require('./config/db.config.js');
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
// Connecting to the database
mongoose.connect(dbConfig.url, {
    useNewUrlParser: true
}).then(() => {
    console.log("Successfully connected to the database");
}).catch(err => {
    console.log('Could not connect to the database.', err);
    process.exit();
});
// define a root/default route
app.get('/', (req, res) => {
    res.json({ "message": "Hello World" });
});
app.post("/api/session", function(req, res, next) {
    if (req.body.username && req.body.password &&
        req.body.username === 'admin' && req.body.password === 'password') {
        const token = jwt.sign({
            userId: 123,
            name: 'Bladimir',
            permission: ['create', 'edit', 'delete']
        }, theSecretKey);

        res.status(201).json({
            token
        })
    } else {
        next();
    }
});
// // custom basic authentication
// app.use(function(req, res, next) {
//     if (req.headers["authorization"]) {
//         const authBase64 = req.headers['authorization'].split(' ');
//         const userPass = base64decode(authBase64[1]);
//         const user = userPass.split(':')[0];
//         const password = userPass.split(':')[1];

//         //
//         if (user === 'admin' && password == '1234') {
//             next();
//             return;
//         }
//     }
//     res.status(401);
//     res.send({
//         error: "Unauthorized "
//     });
// });

// // using basic auth
// app.use(basicAuth({
//     users: {
//         'admin': '1234',
//         'user1': 'supersecret1',
//         'user2': 'supersecret2',
//     }
// }));

// JWT Authentication
app.use(function(req, res, next) {
    if (req.headers["authorization"]) {
        const authToken = req.headers['authorization'].split(' ')[1];
        try {
            jwt.verify(authToken, theSecretKey, (err, decodedToken) => {
                if (err || !decodedToken) {
                    res.status(401);
                    res.json({
                        error: "Unauthorized "
                    });
                }
                console.log('Welcome', decodedToken.name);
                next();
                // if (decodedToken.userId == 123) {
                //   next();
                // }
            });
        } catch (e) {
            next();
        }

    } else {
        res.status(401);
        res.send({
            error: "Unauthorized "
        });
    }
});
const studentRoutes = require('./src/routes/student.routes')
    // using as middleware
app.use('/api/students', studentRoutes)
    // listen for requests
app.listen(port, () => {
    console.log(`Node server is listening on port ${port}`);
});