import { Component, OnInit } from '@angular/core';
import { BreweryService } from './brewery.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  implements OnInit{
 
  title = 'Workshop1'
  breweries: any = [];
  exist :boolean;
  b: any;

  constructor(
    protected breweryService: BreweryService
  ) {
  }
  ngOnInit(): void {
    this.breweryService.getBreweries()
    .subscribe(
      res =>{
        this.breweries = res;
      
      },
      (error)=>{
        console.log(error);
      }
    );
  }

  getDetails(name: any){
    this.breweryService.getOne(name)
    .subscribe(
      res=>{
        this.b = res;
        this.exist = true;
        console.log(this.b);
      },
      (error)=>{
        console.log(error);
      }
    )
    
  }

}
