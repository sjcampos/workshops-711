import { Injectable } from '@angular/core';
import {HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BreweryService {

  constructor(protected http: HttpClient) { }

  getBreweries() {
    return this.http.get('https://api.openbrewerydb.org/breweries?per_page=10');
  }

  getOne(keyword: string){
    return this.http.get(`https://api.openbrewerydb.org/breweries?by_name=${keyword}`);
  }
}
